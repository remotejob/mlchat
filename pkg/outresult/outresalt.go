package outresult

import (
	"log"
	"strings"

	"gitlab.com/remotejob/mlchat/intrernal/config"
	"gitlab.com/remotejob/mlchat/pkg/checkurl"
	"gitlab.com/remotejob/mlchat/pkg/dbhandler"
	"gitlab.com/remotejob/mlchat/pkg/sanitizein"
)

func Append(conf *config.Config, botuuids []string) {

	for _, botuuid := range botuuids {

		insouts := dbhandler.Select(conf.Database, "select textin,textout from "+conf.Constants.Mysql.Table+" where uuid='"+botuuid+"'")

		var dialog [][]string

		var txtin string
		var txtout string

		for _, inout := range insouts {

			txtin = inout[0]
			txtout = inout[1]

			nwords := strings.Fields(txtin)

			if len(nwords) == 1 {

				if ok, ntxtin := checkurl.Check(txtin); ok {

					log.Println("URL", txtin)

					txtin = ntxtin
					nwords = strings.Fields(txtin)

				}

			}
			// nwords = strings.Fields(txtin)

			if strings.HasPrefix(txtin, "Hei") && len(txtin) < 20 || len(nwords) < 2 {

				log.Println("Not included", txtin)

			} else {

				ninout :=[]string{txtin,txtout}

				dialog = append(dialog, ninout)

			}

		}

		if len(dialog) > 6 {

			var previousin string
			var nowin string

			for _, inout := range dialog {

				nowin = inout[0]

				if previousin != nowin {

					in := sanitizein.Sanitize(inout[0])

					_, err := conf.Outfile.WriteString(strings.ToLower(in) + "\n")
					if err != nil {
						log.Fatalf("failed writing to file: %s", err)
					}

					_, err = conf.Outfile.WriteString("> " + inout[1] + "\n")
					if err != nil {
						log.Fatalf("failed writing to file: %s", err)
					}

					// previousin = inout[0]
				} else {

					// previousin = nowin
				}

				previousin = nowin
			}

			// previousin = in

			_, err := conf.Outfile.WriteString("\n")
			if err != nil {
				log.Fatalf("failed writing to file: %s", err)
			}

		}

	}

	// for _, textin := range textins {

	// 	if strings.HasPrefix(textin, "Hei ") && len(textin) < 20 {

	// 		log.Println("Not included", textin)

	// 	} else {

	// 		_, err := conf.Outfile.WriteString("> " + textin + "\n")
	// 		if err != nil {
	// 			log.Fatalf("failed writing to file: %s", err)
	// 		}

	// 	}

	// }
}
