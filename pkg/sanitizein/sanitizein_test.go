package sanitizein

import "testing"

func TestSanitize(t *testing.T) {
	type args struct {
		in string
	}
	// in := "aijaa.. saattais olla helpotusta tarjolla, kiinnostaisko? ;)"
	in := "ooh se tekeekin hyvää,      haluisitko että imen samalla kyrpääsi?"
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"test0",args{in}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Sanitize(tt.args.in)
		})
	}
}
