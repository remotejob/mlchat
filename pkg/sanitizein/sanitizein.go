package sanitizein

import (
	"regexp"
	"strings"
)

func Sanitize(in string) string {

	reg := regexp.MustCompile("[\\p{L}\\d_]+")

	processedString := reg.FindAllString(in, -1)
	clphrase := strings.Join(processedString, " ")

	return clphrase

}
