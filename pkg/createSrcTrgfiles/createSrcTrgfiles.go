package createSrcTrgfiles

import (
	"log"
	"os"
)

func Create(src *os.File, trg *os.File, dialog [][]string) {

	for _, dl := range dialog {

		// if _, err := src.Write([]byte(dl[0] + "\n")); err != nil {
		// 	log.Fatal(err)
		// }
		// if _, err := trg.Write([]byte(dl[1] + "\n")); err != nil {
		// 	log.Fatal(err)
		// }

		if _, err := src.WriteString(dl[0] + "\n"); err != nil {
			log.Fatal(err)
		}
		if _, err := trg.WriteString(dl[1] + "\n"); err != nil {
			log.Fatal(err)
		}
	}

}
