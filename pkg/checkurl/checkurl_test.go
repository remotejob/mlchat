package checkurl

import "testing"

func TestCheck(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
		// {"test0",args{"No%20juu%20niin%20voidaan%20siell%E4%20viestitell%E4%20ns.%20Ilmaiseksi%20%3A%29%29%C3%A1"},false},
		{"test1",args{"Hei%2C%20P%E4iv%E4%21"},false},
		{"test2",args{"Soitatko"},false},
		{"test3",args{"Porvoonkatu%2026%20loviisa%2C%20suostutko%20my%F6s%20nukuman%20alasti%20yhdess%E4%20mun%20kanssa%20paris%E4ngyss%E4%20alasti%20peiton%20alla%20seksin%20j%E4%E4lken%20kun%20ollaan%20naitu%20ja%20suhtessa "},false},

		{"test4",args{"%C4l%E4%20kulta%20ole%20vaikea%2C%20vastaa%20vain%20kysymykseen%21"},false},

		{"test5",args{"Hei%2C%20Aija%21%20T%D6YN%C4KK%D6%20SYV%C4LLE%20pillu"},false},
	

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Check(tt.args.url); got != tt.want {
				t.Errorf("Check() = %v, want %v", got, tt.want)
			}
		})
	}
}
