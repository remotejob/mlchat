package checkurl

import (
	"log"
	"net/url"
	"strings"
)

func Check(urlstr string) (bool, string) {

	clurlstr := strings.Replace(urlstr, "%E4", "%C3%A4", -1)
	clurlstr = strings.Replace(clurlstr, "%F6", "%C3%B6", -1)
	clurlstr = strings.Replace(clurlstr, "%C4", "%C3%84", -1)
	clurlstr = strings.Replace(clurlstr, "%D6", "%C3%96", -1)

	m, err := url.PathUnescape(clurlstr)

	if err != nil {

		log.Println(err)
		log.Println(clurlstr)
	}

	// fmt.Println(m)

	if m == urlstr {

		return false, ""

	}
	return true, m

}
