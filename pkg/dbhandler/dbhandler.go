package dbhandler

import (
	"database/sql"
	"log"
)

func Select(db *sql.DB, stmt string) [][]string {

	var res [][]string

	rows, err := db.Query(stmt)
	if err != nil {
		log.Panicln(err.Error()) // proper error handling instead of panic in your app
	}

	for rows.Next() {

		var textin string
		var textout string
		err = rows.Scan(&textin,&textout)
		if err != nil {
			log.Panicln(err.Error()) // proper error handling instead of panic in your app
		}

		// log.Println(textin)
		dialog := []string{textin,textout}
		res = append(res, dialog)

	}

	return res

}
func SelectOne(db *sql.DB, stmt string) []string {
	var res []string
	rows, err := db.Query(stmt)
	if err != nil {
		log.Panicln(err.Error()) // proper error handling instead of panic in your app
	}
	for rows.Next() {
		var textin string
		err = rows.Scan(&textin)
		if err != nil {
			log.Panicln(err.Error()) // proper error handling instead of panic in your app
		}
		// log.Println(textin)
		res = append(res, textin)
	}
	return res
}