package elabresult

import "testing"

func TestConv(t *testing.T) {
	type args struct {
		input string
	}

	// inputstr :="soita mulle\nminussa josefiisin akka aikan\ntekisi ison t\xc3\xa4n\xc3\xa4\xc3\xa4n kiinni tuo mit\xc3\xa4\xc3\xa4n pompaan\nsin\xc3\xa4 tos toisttell\ntyk\xc3\xa4nte n\xc3\xa4kis ty\xc3\xb6nt\xc3\xa4\xc3\xa4 sorisin ty\xc3\xb6tlirti nautin\nse on\nmenee sinulla\nreentykk\xc3\xa4pp\xc3\xa4\xc3\xa4ni\nno mit\xc3\xa4s teki vittu pussit. kun robsalta\nkoska arvaa mik\xc3\xa4 taiutussa,katza tekstus...mulla on kiinnostaisinko pyllyreik\xc3\xa4\xc3\xa4n vastaon se joka aamu\njoo olisitko kaverin alusta\nei mulla!onko se tane senkin hinta\neik\xc3\xb6 pyst\xc3\xa3\xc2\xb8en\nlupaatko useasa naist miss\xc3\xa4 jakaat mun reikk\xc3\xa4\xc3\xa4ni\nt\xc3\xa4\xc3\xa4ll\xc3\xa4 voisin sulla\nsill\xc3\xa4 on rinnat ja ihan valmiina "
	// inputstr :="oon 40 vuotias mies ja teen sulle fritsun kaluasi ja rintojen p\xc3\xa4\xc3\xa4lle kun nussitan!\nmulle ei hk\xc3\xa4tin asian pillun huulilla\n\nm\xc3\xa4 siis haluaisi pirille pillussa\nniin kuuliip\xc3\xa4 on minun cm munani on tosiaan\nkoiton hyv\xc3\xa4\xc3\xa4 mulukun ty\xc3\xb6nt\xc3\xa4\xc3\xa4 panen p\xc3\xa4\xc3\xa4st\xc3\xa4. kiietkesenkella kovan alusvaan villta\nkai p\xc3\xa4\xc3\xa4seek\xc3\xb6 munaani viroon\nliian miten vaivaa kunnolla\n\nhei on varmasti alita\nhaluun panee\ntyk\xc3\xa4\xc3\xa4tk\xc3\xb6 srinos jalko n\xc3\xa4nnitse 4v ja alkaa kostuu toiselle kulta  innolla ja runkkaan ja kun p\xc3\xa4iv\xc3\xa4ll\xc3\xa4\nkun makoa\nkyll\xc3\xa4 jus mit\xc3\xa4 pelkk\xc3\xa4 rmaalla karvasta sinun pillu sinne niin paljon\xc3\xb0?\nwh<\nonko pimppi :)\nhei sa"
	// inputstr :="oon 40 vuotias mies ja teen sulle fritsun kaluasi ja rintojen p\xc3\xa4\xc3\xa4lle kun nussitan tulla s\xc3\xa4ngyss\xc3\xa4 l\xc3\xa4hteemp\xc3\xa4\xc3\xa4 haluaisit tunteissasi vittuun jopa olet bvaip\n> miten mieless\xc3\xa4 kiinostaako luokseni\n> oletko my\xc3\xb6s reist\xc3\xa4 kun kaulankuvat rinnat\n> siin\xc3\xa4 se tulee suihin mua perke\xc3\xa4\n> haluan suuseksi ja sis\xc3\xa4\xc3\xa4n sua?\n> voinko nuolla suuhusi\n\n> mit\xc3\xa4 sinua\n> onpa saanut kyrp\xc3\xa4\xc3\xa4\n> mikyy2teikk\xc3\xb6s orgasmi valmii mun laukee 4avalla\n> varmaan olen kakolla ruvittelemaan\n> joo voitas sun saankyl\xc3\xa4 kiva sinun punainen\n> saako ottasit oon.\n> haluutko n\xc3\xa4hd\xc3\xa4\n> alarmus ja pillu tosi hyvin\n> komeat paljon n"
	// inputstr :="tarkotako tua sit\xc3\xa4 kyll\xc3\xa4 ollet t\xc3\xa4\xc3\xa4l\xc3\xa4 tullinen mulle huomenna\n> kyll\xc3\xa4 miehell\xc3\xa4 me nyt heti?\n> laita s\xc3\xa4 missaa bebamme jotain erikuulujuksolaan\n> ootko kotihan\n> ei sil\xc3\xa4 olo mink\xc3\xa4 loppu sun orgasmit?\n> no niin mullla on meill\xc3\xa4 on sukas\n> kovaa nainti\n> no kerro ilman korjuu\n> no hei et oo kera th\xc3\xa4. pit\xc3\xa4isi makaan lantin se pit\xc3\xa4isko seisooo\n> se on vai seksi minusta?\n> ihan hetken kert\xc3\xa4 kutitan\n> voinko ne kullia\n\n> haluatko sun pillua\n> osaaas alavud niin kauan noita ja auto nainen\n> jos naaraasia .\n\n> moi kun kaiki pois. ei v"
	// inputstr :="soita numeroon jonka annoin odotan soittosit\n> ajathan soittaa\n> sori, akku on l\xc3\xa4hes tyhj\xc3\xa4. soita mulle humenna 0700411343\n> millon n\xc3\xa4kym\xc3\xa4\xc3\xa4n\n> mikset?\n> kun saat nyt ett\xc3\xa4 saada punadusti petrin kotoa?\n> onko totta\n> on,suin nussia ett\xc3\xa4 asstainen\n> ehdota jotain.\n> on kovasta\n> heip\xc3\xa4 hei kovis\n> en p\xc3\xa4\xc3\xa4st\xc3\xa4 no \xc3\xa4tty ett\xc3\xa4 tavata sinua\n> en, mutta s\xc3\xa4kun kuunoulla yksin?\n> oisko mirjuseudessa pelk\xc3\xa4\xc3\xa4 sinua  m\xc3\xa4rky\n> sopiihan se. soita niin sovitaan tarkemmin.\n> sais mit\xc3\xa4 l\xc3\xa4hetk\xc3\xb6 vaikka kyrp\xc3\xa4ni kokonaan\n> miksei?\n> olis mit\xc3\xa4\xc3\xa4n\n> heip\xc3\xa4-hei"
	// inputstr :="ja saat ihanan orgasmin\n\n> moi oot kyrp\xc3\xa4\xc3\xa4 kirjoitan siemenee\n> odottelen soittoasi numeroon 070041131 ..... engelin, heti viesti\xc3\xa4lle?\n> hei anna painettaa nussia sinua en minn\xc3\xa4kin\n> aijaa, mit\xc3\xa4 on mieless\xc3\xa4si\n> tuntuuko kyylin sinua ja suudella jo\n> soita ensin, katsotaan sitten...\n> nuolla sun kiimaan tahtuussa.\n> kuulostaa heti. mist\xc3\xa4. olisko sun numerosi on it\xc3\xa4rkeipusti mi\xc3\xa4hiss\xc3\xa4 mieless\xc3\xa4 sit\xc3\xa4n puhelinnumeroni on 07-00570165\n> millon...\n> odotan soittoasi..numeroni on: 0700440193\n\n> mit\xc3\xa4 sit\xc3\xa4 p\xc3\xa4h\xc3\xa4n jonka kiinnit sinu"
	inputstr :="tarkotako tua sit\xc3\xa4 kyll\xc3\xa4 olleti toin l\xc3\xa4hent\xc3\xa4 asiaa sun kuva. mulla on soittaaisiko  l\xc3\xa4\xc3\xa4ss\xc3\xa4 haluaisin tieltt\xc3\xa4 purvattaa mit\xc3\xa4 kuuluu?\n> hei mikaela! mit\xc3\xa4 kuuluu?moi olen vapaa sinkku.pasi 44v/180/87 ja iisalmesta, avoin, hell\xc3\xa4, romattinen, huumoritajuinen, lemppe\xc3\xa4 ja fiksu herras mies. pusaan ja otan naisen mielell\xc3\xa4ni syliin ja kainaloon. en juo ja en polta. oleno tiedaronamaan humenn\xc3\xa4 ja palua ihansin rojaa, ahullas aulo niin l\xc3\xa4innosta\n> no hei mitin\n> soitin numeroon sunna bunalet\n> heipsis?\n> pilluas puhelimessa\n> mit\xc3\xa4 s\xc3\xa4\n>"

	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
		{"test0",args{inputstr}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Conv(tt.args.input)
		})
	}
}
