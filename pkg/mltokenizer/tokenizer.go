package mltokenizer

import (
	"strings"

	"github.com/euskadi31/go-tokenizer"
)

func MlTokenize(t tokenizer.Tokenizer, in string) string {

	var out string
	tokens := t.Tokenize(strings.ToLower(in))

	out = strings.Join(tokens, " ")

	return out

}
