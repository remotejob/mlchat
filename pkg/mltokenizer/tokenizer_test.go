package mltokenizer

import (
	"testing"

	// "github.com/ikawaha/kagome/tokenizer"
	"github.com/euskadi31/go-tokenizer"
)

func TestMlTokenize(t *testing.T) {
	type args struct {
		t  tokenizer.Tokenizer
		in string
	}

	tt := tokenizer.New()

	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"test0",args{tt,"I believe life is an intelligent thing: that things aren't random."},"kskskskssk"},
		{"test1",args{tt,"moi anteeksi oletko kauhajoella ? mä haluaisin seksi jos haluat vasta"},"kskskskssk"},
		{"test2",args{tt,"HALUUTKKO HARRSATA SEKSI KOKO MUN KANSSA EI SOITA"},"kskskskssk"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MlTokenize(tt.args.t, tt.args.in); got != tt.want {
				t.Errorf("MlTokenize() = %v, want %v", got, tt.want)
			}
		})
	}
}
