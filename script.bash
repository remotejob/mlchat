
split -l 1872904 nmt_data/train.ask.all
mv xaa nmt_data/train.ask
mv xab tst
split -l 10000 tst
mv xaa  nmt_data/tst2012.ask
mv xab  nmt_data/tst2013.ask
rm tst

split -l 1872904 nmt_data/train.ans.all
mv xaa nmt_data/train.ans
mv xab tst
split -l 10000 tst
mv xaa  nmt_data/tst2012.ans
mv xab  nmt_data/tst2013.ans
rm tst


# tar zcv --exclude='*.all' -f mynmtdata.tar.gz nmt_data/
tar zcfv chat-data.tar.gz chat-data/
scp chat-data.tar.gz 192.168.1.206:toy-ende/

# scp mynmtdata.tar.gz 192.168.1.206:nmt/


python -m nmt.nmt --src=ask --tgt=ans --vocab_prefix=nmt_data/vocab --train_prefix=nmt_data/train --dev_prefix=nmt_data/tst2012 --test_prefix=nmt_data/tst2013 --out_dir=nmt_model --num_train_steps=60000 --steps_per_stats=100 --num_layers=2 --num_units=128 --dropout=0.2 --metrics=bleu

python -m nmt.nmt --attention=scaled_luong --src=ask --tgt=ans --vocab_prefix=nmt_data/vocab --train_prefix=nmt_data/train --dev_prefix=nmt_data/tst2012 --test_prefix=nmt_data/tst2013 --out_dir=nmt_model --num_train_steps=300000 --steps_per_stats=100 --num_layers=2 --num_units=128 --dropout=0.2 --metrics=bleu

python -m nmt.nmt --out_dir=nmt_model --inference_input_file=myinferfile/my_infer_file.ask --inference_output_file=nmt_model/output_infer