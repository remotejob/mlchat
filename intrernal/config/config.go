package config

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"

	_ "github.com/go-sql-driver/mysql"
)

type Constants struct {
	Mysql struct {
		URL string
		// DBName string
		Table string
	}
	Resout struct {
		Outfile string
	}
}
type Config struct {
	Constants
	Database *sql.DB
	Outfile  *os.File
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}
	db, err := sql.Open("mysql", config.Constants.Mysql.URL)
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	// defer db.Close()
	if err != nil {
		return &config, err
	}
	config.Database = db

	file, err := os.OpenFile(config.Constants.Resout.Outfile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	config.Outfile = file

	return &config, err
}
func initViper() (Constants, error) {
	viper.SetConfigName("mlchat.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")             // Search the root directory for the configuration file
	err := viper.ReadInConfig()          // Find and read the config file
	if err != nil {                      // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	// viper.SetDefault("PORT", "8000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}
	var constants Constants

	err = viper.Unmarshal(&constants)
	return constants, err
}
