module gitlab.com/remotejob/mlchat

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/euskadi31/go-tokenizer v0.0.0-20180318172036-8b5faeae2841
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-sql-driver/mysql v1.4.0
	github.com/ikawaha/kagome v1.8.1 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/spf13/cast v1.3.0 // indirect
	github.com/spf13/pflag v1.0.3 // indirect
	github.com/spf13/viper v1.2.1
	gitlab.com/remotejob/tensorflowgo v0.0.0
	golang.org/x/net v0.0.0-20181023162649-9b4f9f5ad519 // indirect
	golang.org/x/sys v0.0.0-20181026203630-95b1ffbd15a5 // indirect
	google.golang.org/appengine v1.2.0 // indirec
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
