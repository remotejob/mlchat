package main

import (
	"log"
	"os"
	"strings"

	"gitlab.com/remotejob/mlchat/intrernal/config"
	"gitlab.com/remotejob/mlchat/pkg/checkurl"
	"gitlab.com/remotejob/mlchat/pkg/createSrcTrgfiles"
	"gitlab.com/remotejob/mlchat/pkg/dbhandler"
	"gitlab.com/remotejob/mlchat/pkg/sanitizein"
)

func main() {

	conf, err := config.New()
	if err != nil {
		log.Panicln("Configuration error", err)

	}

	srcfile, err := os.OpenFile("chat-data/src-train.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	trgfile, err := os.OpenFile("chat-data/tgt-train.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	// insouts := dbhandler.Select(conf.Database, "select textin,textout from "+conf.Constants.Mysql.Table+" where known=1")
	insouts := dbhandler.Select(conf.Database, "select textin,textout from "+conf.Constants.Mysql.Table)

	var dialog [][]string

	var txtin string
	var txtout string

	// tk := tokenizer.New()

	uniquemap := make(map[string]struct{})

	for _, inout := range insouts {

		txtin = inout[0]
		txtout = inout[1]

		nwords := strings.Fields(txtin)

		if len(nwords) == 1 {

			if ok, ntxtin := checkurl.Check(txtin); ok {

				// log.Println("URL", txtin)

				txtin = ntxtin
				nwords = strings.Fields(txtin)

			}

		}

		if strings.HasPrefix(txtin, "Hei") && len(txtin) < 20 || len(nwords) < 2 {

			// log.Println("Not included", txtin)

		} else {

			// ninout := []string{mltokenizer.MlTokenize(tk, txtin), mltokenizer.MlTokenize(tk, txtout)}

			trtxtin := strings.TrimSpace(txtin)
			trtxtout := strings.TrimSpace(txtout)

			if len(trtxtin) > 1 && len(trtxtout) > 1 {

				trtxtin = strings.ToLower(sanitizein.Sanitize(trtxtin))
				trtxtout = strings.ToLower(sanitizein.Sanitize(trtxtout))

				if _, ok := uniquemap[trtxtin+trtxtout]; !ok {

					ninout := []string{trtxtin, trtxtout}

					dialog = append(dialog, ninout)

					uniquemap[trtxtin+trtxtout] = struct{}{}

				} else {
					// log.Println("used before", trtxtin,len(uniquemap))

					// uniquemap[trtxtin+trtxtout] = struct{}{}

				}

			}

		}

	}

	createSrcTrgfiles.Create(srcfile, trgfile, dialog)

}
