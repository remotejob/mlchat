package main

import (
	"log"

	"gitlab.com/remotejob/mlchat/intrernal/config"
	"gitlab.com/remotejob/mlchat/pkg/dbhandler"
	"gitlab.com/remotejob/mlchat/pkg/outresult"
)

func main() {
	configuration, err := config.New()
	if err != nil {
		log.Panicln("Configuration error", err)

	}

	// sqlres := dbhandler.Select(configuration.Database, "select DISTINCT textin from "+configuration.Constants.Mysql.Table+" limit 10000")
	sqlres := dbhandler.SelectOne(configuration.Database, "select uuid from botuuid")

	outresult.Append(configuration, sqlres)
}
