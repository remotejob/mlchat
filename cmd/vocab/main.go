package main

import (
	"bufio"
	"log"
	"os"
	"strings"
)

func main() {

	vocabask := make(map[string]int, 0)
	vocabans := make(map[string]int, 0)

	srcfile, err := os.Open("nmt_data/train.ask.all")
	if err != nil {
		log.Fatal(err)
	}

	tgtfile, err := os.Open("nmt_data/train.ans.all")
	if err != nil {
		log.Fatal(err)
	}
	vocabaskfile, err := os.OpenFile("nmt_data/vocab.ask", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	vocabansfile, err := os.OpenFile("nmt_data/vocab.ans", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer srcfile.Close()
	defer tgtfile.Close()
	defer vocabaskfile.Close()
	defer vocabansfile.Close()

	scanner := bufio.NewScanner(srcfile)
	for scanner.Scan() {
		// fmt.Println(scanner.Text())
		words := strings.Fields(scanner.Text())

		for _, word := range words {

			if val, ok := vocabask[word]; ok {

				vocabask[word] = val + 1
			} else {

				vocabask[word] = 0

			}
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	scanner = bufio.NewScanner(tgtfile)
	for scanner.Scan() {
		// fmt.Println(scanner.Text())
		words := strings.Fields(scanner.Text())

		for _, word := range words {

			if val, ok := vocabans[word]; ok {

				vocabans[word] = val + 1
			} else {

				vocabans[word] = 0

			}
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for k, v := range vocabask {

		if v > 2 {
			if _, err := vocabaskfile.Write([]byte(k + "\n")); err != nil {
				log.Fatal(err)
			}
		}
	}

	for k, v := range vocabans {

		if v > 2 {
			if _, err := vocabansfile.Write([]byte(k + "\n")); err != nil {
				log.Fatal(err)
			}
		}
	}

}
