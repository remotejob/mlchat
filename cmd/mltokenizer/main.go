package main

import (
	"log"
	"os"
	"strings"

	"github.com/euskadi31/go-tokenizer"
	"gitlab.com/remotejob/mlchat/intrernal/config"
	"gitlab.com/remotejob/mlchat/pkg/checkurl"
	"gitlab.com/remotejob/mlchat/pkg/createSrcTrgfiles"
	"gitlab.com/remotejob/mlchat/pkg/dbhandler"
	"gitlab.com/remotejob/mlchat/pkg/mltokenizer"
)

func main() {

	conf, err := config.New()
	if err != nil {
		log.Panicln("Configuration error", err)

	}

	srcfile, err := os.OpenFile("nmt_data/train.ask.all", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	trgfile, err := os.OpenFile("nmt_data/train.ans.all", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	insouts := dbhandler.Select(conf.Database, "select textin,textout from "+conf.Constants.Mysql.Table+" where known=1")

	var dialog [][]string

	var txtin string
	var txtout string

	tk := tokenizer.New()

	for _, inout := range insouts {

		txtin = inout[0]
		txtout = inout[1]

		nwords := strings.Fields(txtin)

		if len(nwords) == 1 {

			if ok, ntxtin := checkurl.Check(txtin); ok {

				log.Println("URL", txtin)

				txtin = ntxtin
				nwords = strings.Fields(txtin)

			}

		}

		if strings.HasPrefix(txtin, "Hei") && len(txtin) < 20 || len(nwords) < 2 {

			log.Println("Not included", txtin)

		} else {

			ninout := []string{mltokenizer.MlTokenize(tk, txtin), mltokenizer.MlTokenize(tk, txtout)}

			dialog = append(dialog, ninout)

		}

	}

	createSrcTrgfiles.Create(srcfile, trgfile, dialog)

}
